#!/usr/bin/env node
import 'source-map-support/register';

import * as cdk from 'aws-cdk-lib';
import { EventbridgeCdkStack } from '../lib/eventbridge-cdk-stack';

const app = new cdk.App();
cdk.Tags.of(app).add("ATag", new Date().toISOString());

new EventbridgeCdkStack(app, 'EventbridgeCdkStack', {
});