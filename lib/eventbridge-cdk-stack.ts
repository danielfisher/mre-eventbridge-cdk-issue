import { Stack, aws_events, StackProps, aws_s3 } from 'aws-cdk-lib';
import { Construct } from 'constructs';

export class EventbridgeCdkStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);
    new aws_events.EventBus(this, 'bus', {
      eventBusName: 'ExampleBus'
    });
  }
}
